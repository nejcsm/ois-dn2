function divElementEnostavniTekst(sporocilo) {
  return $('<div style="font-weight: bold"></div>').text(sporocilo);
}
var ime;
var kanal;
function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}
var grdeBesede;

$.get('besede.txt', function(data) {
   
  //procesiraj file
  grdeBesede = data.split("\n");
  $('#div').html(data.replace('n','<br />'));
   
}, 'text');

function procesiranjeGrdihBesed(sporocilo) {
  for(var j = 0; j < grdeBesede.length; j++) {
    sporocilo = sporocilo.replace(new RegExp('\\b' + grdeBesede[j] + '\\b', 'g'),new Array(grdeBesede[j].length + 1).join("*"));
  }
  return sporocilo;
}



function divElementTeks(sporocilo) {
  return $('<div style="font-weight: bold"></div>').html(sporocilo);  
}

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;
  
  sporocilo = procesiranjeGrdihBesed(sporocilo);
  var  zVecjem = sporocilo.replace(/</g,"&lt");
  var  zManjse = zVecjem.replace(/</g,"&gt");
  
  var pomezik = zManjse.replace(/\;\)/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png"/>');
  var smajli = pomezik.replace(/\:\)/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png"/>');
  var poljub = smajli.replace(/\:\*/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png"/>');
  var uzaljeni = poljub.replace(/\:\(/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png"/>');
  var like = uzaljeni.replace(/\(\Y\)/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png"/>');
  
  


  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {

 
  klepetApp.posljiSporocilo($('#kanal').text(), like);
 
  $('#sporocila').append(divElementTeks(like));
  
  $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
   


  }

  $('#poslji-sporocilo').val('');
}

var socket = io.connect();




$(document).ready(function() {
  var klepetApp = new Klepet(socket);
 
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
     
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
      $('#kanal').text(rezultat.vzdevek + ' @ ' + kanal);
      ime = rezultat.vzdevek;
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });
  
  socket.on('pridruzitevOdgovor', function(rezultat) {
    //ime=rezultat.vzdevek;
    kanal=rezultat.kanal;
    $('#kanal').text(ime + ' @ ' + kanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });

  socket.on('sporocilo', function (sporocilo) {
    var novElement = $('<div style="font-weight: bold"></div>').html(sporocilo.besedilo);
    $('#sporocila').append(novElement);
  });
  
   socket.on('uporabniki', function(upi) {
    $('#seznam-uporabnikov-na-kanalu').empty();

    for(var u = 0; u < upi.length;u++) {
      
        $('#seznam-uporabnikov-na-kanalu').append(divElementEnostavniTekst(upi[u]));
     
    }
  });
  
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });

  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki');
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});